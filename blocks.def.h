//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon	Command												Interval	Signal*/
	{"", 	"mem",													2,				0},
	{"", 	"cpu",													2,				0},
	{"", 	"temp",													2,				0},
	{"", 	"vol", 													0,			 10},
	{"", 	"wlan", 												60,			  0},
	{"", 	"batt",													60,			 	0},
	{"",	"date '+%a %d %b %H:%M'",				4,				0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
